<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package Swatched
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function swatched_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'swatched_jetpack_setup' );
